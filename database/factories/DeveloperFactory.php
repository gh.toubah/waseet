<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Developer>
 */
class DeveloperFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => fake()->name(),
            'profile_picture' => fake()->imageUrl(),
            'rate' => $this->faker->numberBetween(1, 5),
            'type' => $this->faker->randomElement(['ui/ux','full stack','frontend','backend','designer']),
        ];
    }
}
