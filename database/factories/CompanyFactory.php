<?php

namespace Database\Factories;
use App\Models\company;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Company>
 */
class CompanyFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'address' => $this->faker->address,
            'name' => $this->faker->company,
            'profile_picture' => fake()->imageUrl(),
            'email' => $this->faker->unique()->safeEmail,
            'email_verified_at' => now(),
            'phone' => $this->faker->numberBetween(1000000000, 9999999999),
            'description' => $this->faker->sentence,
            'type' => $this->faker->randomElement(['transportation','security','cleaning','health insurance']),
        ];
    }
}
