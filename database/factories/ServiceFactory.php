<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Service>
 */
class ServiceFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => fake()->optional()->name(),
            'phone' => $this->faker->numberBetween(1000000000, 9999999999),
            'description' => $this->faker->sentence,
            'cv' => $this->faker->optional()->url,
            'type' => $this->faker->randomElement(['developer', 'design','sales&marketing','teach&learn']),
            'category_type' => $this->faker->randomElement(['company', 'individual']),
        ];
    }
}
