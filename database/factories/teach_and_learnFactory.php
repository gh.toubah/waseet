<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\teach_and_learn>
 */
class teach_and_learnFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'instructor' => $this->faker->name,
            'subject' => $this->faker->word,
            'level' => $this->faker->randomElement(['beginner', 'intermediate', 'advanced']),
            'name' => fake()->name(),
            'profile_picture' => fake()->imageUrl(),
            'rate' => $this->faker->numberBetween(1, 5),
            'type' => $this->faker->randomElement(['remotly','in school']),

        ];
    }
}
