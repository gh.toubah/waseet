<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Sales_and_Marketing>
 */
class Sales_and_MarketingFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => fake()->name(),
            'profile_picture' => fake()->imageUrl(),
            'rate' => $this->faker->numberBetween(1, 5),
            'type' => $this->faker->randomElement(['market','sales']),
        ];
    }
}
