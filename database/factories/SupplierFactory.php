<?php

namespace Database\Factories;
use App\Models\supplier;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Supplier>
 */
class SupplierFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'type' => $this->faker->randomElement(['company', 'individual']),
            'project_history' => $this->faker->optional()->sentence,
            'about' => $this->faker->text(100),
            'profile_picture' => fake()->imageUrl(),
            'cv' => $this->faker->optional()->url,
        ];
    }
}
