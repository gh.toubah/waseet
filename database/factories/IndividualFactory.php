<?php

namespace Database\Factories;
use App\Models\individual;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Individual>
 */
class IndividualFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => fake()->name(),
            'status' => $this->faker->randomElement(['active', 'inactive']),
            'profile_picture' => fake()->imageUrl(),
        ];
    }
}
