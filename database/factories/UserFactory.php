<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\User>
 */
class UserFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => fake()->name(),
            'email' => fake()->unique()->safeEmail(),
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'date' => $this->faker->date(),
            'cv' => $this->faker->optional()->url,
            'phone_number' => $this->faker->phoneNumber,
            'address' => $this->faker->address,
            'education' => $this->faker->sentence(3),
            'experiences' => $this->faker->optional()->sentence(6),
            'previous_courses' => $this->faker->optional()->sentence(6),
            'remember_token' => Str::random(10),
            'rate' => $this->faker->numberBetween(1, 5),
        ];
    }

    /**
     * Indicate that the model's email address should be unverified.
     *
     * @return $this
     */
    public function unverified(): static
    {
        return $this->state(fn (array $attributes) => [
            'email_verified_at' => null,
        ]);
    }
}
