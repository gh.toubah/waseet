<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
       \App\Models\User::factory(30)->create();
       \App\Models\client::factory(30)->create();
       \App\Models\supplier::factory(30)->create();
       \App\Models\offer::factory(30)->create();
       \App\Models\individual::factory(30)->create();
       \App\Models\contract::factory(30)->create();
       \App\Models\company::factory(30)->create();
       \App\Models\employee::factory(30)->create();
       \App\Models\all::factory(30)->create();
       \App\Models\service::factory(30)->create();
       \App\Models\teach_and_learn::factory(30)->create();
       \App\Models\sales_and_marketing::factory(30)->create();
       \App\Models\developer::factory(30)->create();
        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

   /* $this->call([
     ClientSeeder::class
    ]);*/
}}
