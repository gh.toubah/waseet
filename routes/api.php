<?php

use App\Http\Controllers\AllController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\CombinedController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\comp_indivController;
use App\Http\Controllers\ContractController;
use App\Http\Controllers\CvController;
use App\Http\Controllers\DesignController;
use App\Http\Controllers\DeveloperController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\IndividualController;
use App\Http\Controllers\OfferController;
use App\Http\Controllers\OtherController;
use App\Http\Controllers\SalesMarketingController;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\TeachLearnController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SupplierController;
use App\Http\Controllers\ReviewController;
use App\Http\Controllers\authwController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group([
    'middleware' => 'api',
    'prefix' => 'auth',
], function ($router) {
    Route::post('/login', [AuthController::class, 'login']);
    Route::post('/register', [AuthController::class, 'register']);
    Route::post('/logout', [AuthController::class, 'logout']);
    Route::post('/refresh', [AuthController::class, 'refresh']);
    Route::get('/user-profile', [AuthController::class, 'userProfile']);
    Route::post('/resetpassword', [AuthController::class, 'resetPassword']);

     //web
     Route::post('/logins', [authwController::class, 'login']);
     Route::post('/registers', [authwController::class, 'register']);

});

Route::group(['middleware' => ['jwt.verify']], function() {

    Route::get('/suppliers', [SupplierController::class, 'index']);
    Route::post('/suppliers/store', [SupplierController::class, 'store']);
    Route::get('suppliers/show/{id}', [SupplierController::class, 'show']);
    Route::put('suppliers/update/{id}', [SupplierController::class, 'update']);
    Route::delete('suppliers/delete/{id}', [SupplierController::class, 'destroy']);

    Route::get('/clients', [ClientController::class, 'index']);
    Route::post('/clients/store', [ClientController::class, 'store']);
    Route::get('clients/show/{id}', [ClientController::class, 'show']);
    Route::put('clients/update/{id}', [ClientController::class, 'update']);
    Route::delete('clients/delete/{id}', [ClientController::class, 'destroy']);

    Route::get('/individuals', [IndividualController::class, 'index']);
    Route::post('/individuals/store', [IndividualController::class, 'store']);
    Route::get('individuals/show/{id}', [IndividualController::class, 'show']);
    Route::put('individuals/update/{id}', [IndividualController::class, 'update']);
    Route::delete('individuals/delete/{id}', [IndividualController::class, 'destroy']);

    Route::get('/companies', [CompanyController::class, 'index']);
    Route::post('/companies/store', [CompanyController::class, 'store']);
    Route::get('companies/show/{id}', [CompanyController::class, 'show']);
    Route::put('companies/update/{id}', [CompanyController::class, 'update']);
    Route::delete('companies/delete/{id}', [CompanyController::class, 'destroy']);

    Route::get('/employees', [EmployeeController::class, 'index']);
    Route::post('/employees/store', [EmployeeController::class, 'store']);
    Route::get('employees/show/{id}', [EmployeeController::class, 'show']);
    Route::put('employees/update/{id}', [EmployeeController::class, 'update']);
    Route::delete('employees/delete/{id}', [EmployeeController::class, 'destroy']);

    Route::get('/contracts', [ContractController::class, 'index']);
    Route::post('/contracts/store', [ContractController::class, 'store']);
    Route::get('contracts/show/{id}', [ContractController::class, 'show']);
    Route::put('contracts/update/{id}', [ContractController::class, 'update']);
    Route::delete('contracts/delete/{id}', [ContractController::class, 'destroy']);

    Route::get('/offers', [OfferController::class, 'index']);
    Route::post('/offers/store', [OfferController::class, 'store']);
    Route::get('offers/show/{id}', [OfferController::class, 'show']);
    Route::put('offers/update/{id}', [OfferController::class, 'update']);
    Route::delete('offers/delete/{id}', [OfferController::class, 'destroy']);


    Route::get('/designers', [DesignController::class, 'index']);
    Route::post('/designers/store', [DesignController::class, 'store']);
    Route::get('designers/show/{id}', [DesignController::class, 'show']);
    Route::put('designers/update/{id}', [DesignController::class, 'update']);
    Route::delete('designers/delete/{id}', [DesignController::class, 'destroy']);

    Route::get('/developers', [DeveloperController::class, 'index']);
    Route::post('/developers/store', [DeveloperController::class, 'store']);
    Route::get('developers/show/{id}', [DeveloperController::class, 'show']);
    Route::put('developers/update/{id}', [DeveloperController::class, 'update']);
    Route::delete('developers/delete/{id}', [DeveloperController::class, 'destroy']);

    Route::get('/salesmarketing', [SalesMarketingController::class, 'index']);
    Route::post('/salesmarketing/store', [SalesMarketingController::class, 'store']);
    Route::get('salesmarketing/show/{id}', [SalesMarketingController::class, 'show']);
    Route::put('salesmarketing/update/{id}', [SalesMarketingController::class, 'update']);
    Route::delete('salesmarketing/delete/{id}', [SalesMarketingController::class, 'destroy']);

    Route::get('/teachlearn', [TeachLearnController::class, 'index']);
    Route::post('/teachlearn/store', [TeachLearnController::class, 'store']);
    Route::get('teachlearn/show/{id}', [TeachLearnController::class, 'show']);
    Route::put('teachlearn/update/{id}', [TeachLearnController::class, 'update']);
    Route::delete('teachlearn/delete/{id}', [TeachLearnController::class, 'destroy']);


    Route::get('/others', [OtherController::class, 'index']);
    Route::post('/others/store', [OtherController::class, 'store']);
    Route::get('others/show/{id}', [OtherController::class, 'show']);
    Route::put('others/update/{id}', [OtherController::class, 'update']);
    Route::delete('others/delete/{id}', [OtherController::class, 'destroy']);


    Route::get('/all', [AllController::class, 'index']);
    Route::post('/all/store', [AllController::class, 'store']);


    Route::get('/services', [ServiceController::class, 'index']);
    Route::post('/services/store', [ServiceController::class, 'store']);
    Route::get('services/show', [ServiceController::class, 'show']);
    Route::put('services/update/{id}', [ServiceController::class, 'update']);
    Route::delete('services/delete/{id}', [ServiceController::class, 'destroy']);


    //AI
    Route::post('/analyze_reviews', [ReviewController::class, 'analyzeReviews']);
    Route::post('/save_to_pdf', [CvController::class, 'saveToPDF']);

   Route::post('/analyze-sentiment', [CombinedController::class, 'getSentimentScore']);
   Route::get('/contracts', [CombinedController::class, 'index']);
   Route::post('/contracts/store', [CombinedController::class, 'store']);
   Route::get('/contracts/{id}', [CombinedController::class, 'show']);
   Route::put('/contracts/{id}', [CombinedController::class, 'update']);
   Route::delete('/contracts/{id}', [CombinedController::class, 'destroy']);

   //web
   Route::get('/users', [UserController::class, 'index']);
   Route::get('users/show/{id}', [UserController::class, 'show']);
   Route::put('users/update/{id}', [UserController::class, 'update']);
   Route::delete('users/delete/{id}', [UserController::class, 'destroy']);

});
