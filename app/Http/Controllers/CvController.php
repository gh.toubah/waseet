<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class CVController extends Controller
{
    public function saveToPdf(Request $request)
    {
        // Validate the request
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'email' => 'required|email',
            'phone_number' => 'required|string',
            'add' => 'required|string',
            'objective' => 'required|string',
            'education' => 'required|string',
            'previouse_course' => 'required|string',
            'exp' => 'required|string',
            'skills' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 404);
        }

        // Collect validated data
        $validatedData = $request->all();

        try {
            $client = new Client();
            $url = 'http://192.168.1.65:8080/save_to_pdf';  // FastAPI endpoint

            $response = $client->post($url, [
                'json' => $validatedData,
                'timeout' => 30,
                'debug' => true,

            ]);

            $data = $response->getBody()->getContents();
            $filename = 'generated_cv.pdf';

            return response($data)
            ->header('Content-Type', 'application/pdf')
            ->header('Content-Disposition', "attachment; filename=\"{$filename}\"");
    } catch (\GuzzleHttp\Exception\RequestException $e) {
        Log::error('CV generation failed: ' . $e->getMessage());
        Log::error('Request Exception Trace: ' . $e->getTraceAsString());
        if ($e->hasResponse()) {
            Log::error('Response: ' . $e->getResponse()->getBody()->getContents());
        }
        return response()->json(['error' => 'Failed to generate CV'], 500);
    } catch (\Exception $e) {
        Log::error('General exception: ' . $e->getMessage());
        Log::error('Exception Trace: ' . $e->getTraceAsString());
        return response()->json(['error' => 'Failed to generate CV'], 500);
    }
}





   /* private $client;

    public function __construct()
    {
        $this->client = new Client();
    }

    public function saveToPdf(Request $request)
    {
        $fastApiUrl = 'http://192.168.1.65:8080/save_to_pdf'; // Update this with your FastAPI URL

        try {
            $response = $this->client->post($fastApiUrl, [
                'json' => $request->all()
            ]);

            $content = $response->getBody()->getContents();
            $filename = 'generated_cv.pdf';

            return response($content)
                ->header('Content-Type', 'application/pdf')
                ->header('Content-Disposition', "attachment; filename=\"{$filename}\"");
        } catch (\Exception $e) {
            return response()->json(['error' => 'Failed to generate CV.'], 500);
        }
    }*/
}
