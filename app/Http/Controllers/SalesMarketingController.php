<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use App\Models\sales_and_marketing;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SalesMarketingController extends Controller
{
    public function index()
    {
       $salesmarketing=sales_and_marketing::get();
       return response()->json(['message' => 'OK',
       'sales_and_marketing' => $salesmarketing
        ], 200);

    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'type' => 'required|string|max:10',
            'name'=>'required|string|max:255',
            'profile_picture'=>'required|file|mimes:jpeg,png,jpg,gif,svg|max:2048',
           'service_id'=>'nullable|integer|exists:services,id',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 404);
        }
        $profile_picture_path = $request->file('profile_picture')->store('public/profile_pictures');
        $profile_picture_url = asset(Storage::url($profile_picture_path)); // Generate full URL

        $salesmarketing = sales_and_marketing::create($request->all(),[
            'profile_picture' => $profile_picture_url,
        ]);
        if($salesmarketing){
            return response()->json($salesmarketing, 200);
        }
        //unable to create sales_and_marketing
        return response()->json("Bad Request", 404);
    }

    public function show($id)
    {
        try{  $salesmarketing= sales_and_marketing::findOrFail($id);}
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e){
            return response()->json("Bad Request", 404);
        }
        return response()->json($salesmarketing, 200);
    }


    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'type' => 'required|string|max:10',
            'name'=>'required|string|max:255',
            'profile_picture'=>'required|file|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'service_id'=>'nullable|integer|exists:services,id',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 404);
        }
        $profile_picture_path = $request->file('profile_picture')->store('public/profile_pictures');
        $profile_picture_url = asset(Storage::url($profile_picture_path)); // Generate full URL

        try{
        $salesmarketing = sales_and_marketing::findOrFail($id);}
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e){
            return response()->json("Bad Request", 404);
        }

        $salesmarketing->update($request->all(),[
            'profile_picture' => $profile_picture_url,
        ]);
        if($salesmarketing){
            return response()->json(['message' => 'Updated Successfully',
            'sales_and_marketing' => $salesmarketing
             ], 200);
        }
    }

    public function destroy($id)
    {
        try {
        $salesmarketing= sales_and_marketing::findOrFail($id);}
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response("Bad Request", 404);
        }
       $salesmarketing->delete($id);
       if($salesmarketing){
        return response('Deleted Successfully', 200);
    }
    }



}
