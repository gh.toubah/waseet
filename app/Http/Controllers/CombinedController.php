<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use App\Rules\ValidDateFormat;
use App\Models\Contract;
use Tymon\JWTAuth\Facades\JWTAuth;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use App\Models\Offer;
use Illuminate\Http\Request;

class CombinedController extends Controller
{
        // Sentiment Analysis Functionality
        public function getSentimentScore(Request $request)
    {
        $validator=Validator::make($request->all(),
            ['text'=>'required|string',]);

            if ($validator->fails()) {
                return response()->json($validator->errors()->toJson(), 404);
            }


        $validatedData = $request->input('text');

        try {
                $client = new Client();
                $url = 'http://192.168.1.131:8080/analyze-sentiment/';  // Python API endpoint

                $response = $client->post($url, [
                    'json' => ['text' => $validatedData],
                    'timeout'=>30,
                ]);


                    $data = json_decode($response->getBody()->getContents(), true);

                    return response()->json($data, $response->getStatusCode());
                } catch (\GuzzleHttp\Exception\RequestException $e) {
                    Log::error('Sentiment analysis failed: ' . $e->getMessage());
                    return response()->json(['error' => 'Failed to analyze sentiment'], 500);
                }catch (\Exception $e) {
                    Log::error('general Exception: ' . $e->getMessage());
                    return response()->json(['error' => 'Failed to analyze sentiment'], 500);
            }
        }



        // Contract Management Functionality
        public function index()
        {
            $user = JWTAuth::parseToken()->authenticate();
            $contracts = Contract::where('user_id', $user->id)->get();

            return response()->json(['message' => 'OK', 'contracts' => $contracts], 200);
        }

        public function store(Request $request)
        {
            $validator = Validator::make($request->all(), [
                'name' => 'required|string|between:2,100',
                'status' => 'required|string|max:10',
                'price' => 'required|integer|between:100,15000000',
                'description' => 'required|string|max:255',
                'notes' => 'required|string|max:255',
                'expected_duration' => ['required', 'date', new ValidDateFormat],
                'offer_id' => 'nullable|integer|exists:offers,id',
                'service_id' => 'nullable|integer|exists:services,id',
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors()->toJson(), 404);
            }

            // Get sentiment score
            $rate = $this->getSentimentScore($request->notes);

            $user = JWTAuth::parseToken()->authenticate();
            $contract = Contract::create(array_merge(
                $request->all(),
                ['user_id' => $user->id, 'rate' => $rate]
            ));

            if ($contract) {
                return response()->json($contract, 200);
            }

            return response()->json("Bad Request", 404);
        }

        public function show($id)
        {
            try {
                $contract = Contract::findOrFail($id);
            } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
                return response()->json("Bad Request", 404);
            }
            return response()->json($contract, 200);
        }

        public function update(Request $request, $id)
        {
            $validator = Validator::make($request->all(), [
                'name' => 'required|string|between:2,100',
                'status' => 'required|string|max:10',
                'price' => 'required|integer|between:100,15000000',
                'description' => 'required|string|max:255',
                'notes' => 'required|string|max:255',
                'expected_duration' => ['required', 'date', new ValidDateFormat],
                'offer_id' => 'nullable|integer|exists:offers,id',
                'service_id' => 'nullable|integer|exists:services,id',
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors()->toJson(), 404);
            }

            try {
                $contract = Contract::findOrFail($id);
            } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
                return response()->json("Bad Request", 404);
            }

            // Get sentiment score
            $rate = $this->getSentimentScore($request->notes);

            $contract->update(array_merge($request->all(), ['rate' => $rate]));
            if ($contract) {
                return response()->json(['message' => 'Updated Successfully', 'contract' => $contract], 200);
            }
        }

        public function destroy($id)
        {
            try {
                $contract = Contract::findOrFail($id);
            } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
                return response("Bad Request", 404);
            }

            $contract->delete();
            return response('Deleted Successfully', 200);
        }
    }


