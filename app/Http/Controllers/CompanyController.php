<?php

namespace App\Http\Controllers;
use App\Models\company;
use App\Models\client;
use App\Models\supplier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class CompanyController extends Controller
{

    public function index()
    {
       $companies=company::get();
       return response()->json(['message' => 'OK',
       'companies' => $companies
        ], 200);

    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|between:2,100',
            'type' => 'required|string|max:20',
            'address'=>'required|string|max:255',
            'email'=>'required|string|email|max:100|unique:companies,email',
            'phone'=>'required|string|max:9999999999',
            'profile_picture'=>'required|file|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'description'=>'required|string|max:255',
            'client_id'=>'nullable|integer|exists:clients,id',
            'supplier_id'=>'nullable|integer|exists:suppliers,id'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 404);
        }
        $profile_picture_path = $request->file('profile_picture')->store('public/profile_pictures');
        $profile_picture_url = asset(Storage::url($profile_picture_path)); // Generate full URL

        $company = company::create($request->all(),[
            'profile_picture' => $profile_picture_url,
        ]);
        if($company){
            return response()->json($company, 200);
        }
        return response()->json("Bad Request", 404);
    }


    public function show($id)
    {
        try{    $company= company::findOrFail($id);}
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e){
            return response()->json("Bad Request", 404);
        }
        return response()->json($company, 200);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|between:2,100',
            'type' => 'required|string|max:20',
            'address'=>'required|string|max:255',
            'email'=>'required|string|email|max:100|unique:companies,email',
            'phone'=>'required||string|max:9999999999',
            'profile_picture'=>'required|file|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'description'=>'required|string|max:255',
           'client_id'=>'nullable|integer|exists:clients,id',
           'supplier_id'=>'nullable|integer|exists:suppliers,id'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }
        $profile_picture_path = $request->file('profile_picture')->store('public/profile_pictures');
        $profile_picture_url = asset(Storage::url($profile_picture_path)); // Generate full URL

        try{
            $company = company::findOrFail($id);}
            catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e){
                return response()->json("Bad Request", 404);
            }

            $company->update($request->all(),[
                'profile_picture' => $profile_picture_url,
            ]);
            if($company){
                return response()->json(['message' => 'Updated Successfully',
                'company' => $company
                 ], 200);
        }
       // return response()->json($validator->errors(), 422);
    }

    public function destroy($id)
    {
        try {
            $company = company::findOrFail($id);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response("Bad Request", 404);
        }

        $company->delete($id);
        return response('Deleted Successfully', 200);
    }
}
