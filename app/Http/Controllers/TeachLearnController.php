<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use App\Models\teach_and_learn;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class TeachLearnController extends Controller
{
    public function index()
    {
       $teachlearn=teach_and_learn::get();
       return response()->json(['message' => 'OK',
       'teach_and_learn' => $teachlearn
        ], 200);

    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'type' => 'required|string|max:10',
            'name'=>'required|string|max:255',
            'profile_picture'=>'required|file|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'level'=> 'required|string|max:255',
            'instructor'=> 'required|string|max:255',
            'subject'=> 'required|string|max:255',
           'service_id'=>'nullable|integer|exists:services,id',

        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 404);
        }
        $profile_picture_path = $request->file('profile_picture')->store('public/profile_pictures');
        $profile_picture_url = asset(Storage::url($profile_picture_path)); // Generate full URL

        $teachlearn = teach_and_learn::create($request->all(),[
            'profile_picture' => $profile_picture_url,
        ]);
        if($teachlearn){
            return response()->json($teachlearn, 200);
        }
        //unable to create teach_and_learn
        return response()->json("Bad Request", 404);
    }

    public function show($id)
    {
        try{  $teachlearn= teach_and_learn::findOrFail($id);}
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e){
            return response()->json("Bad Request", 404);
        }
        return response()->json($teachlearn, 200);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'type' => 'required|string|max:10',
            'name'=>'required|string|max:255',
            'profile_picture'=>'required|file|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'level'=> 'required|string|max:255',
            'instructor'=> 'required|string|max:255',
            'subject'=> 'required|string|max:255',
            'service_id'=>'nullable|integer|exists:services,id',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 404);
        }
        $profile_picture_path = $request->file('profile_picture')->store('public/profile_pictures');
        $profile_picture_url = asset(Storage::url($profile_picture_path)); // Generate full URL
        try{
        $teachlearn = teach_and_learn::findOrFail($id);}
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e){
            return response()->json("Bad Request", 404);
        }

        $teachlearn->update($request->all(),[
            'profile_picture' => $profile_picture_url,
        ]);
        if($teachlearn){
            return response()->json(['message' => 'Updated Successfully',
            'teach_and_learn' => $teachlearn
             ], 200);
        }
    }

    public function destroy($id)
    {
        try {
        $teachlearn= teach_and_learn::findOrFail($id);}
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response("Bad Request", 404);
        }
       $teachlearn->delete($id);
       if($teachlearn){
        return response('Deleted Successfully', 200);
    }
    }
}
