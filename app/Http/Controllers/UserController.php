<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Rules\ValidDateFormat;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
       $user=User::get();
       return response()->json(['message' => 'OK',
       'user' => $user
        ], 200);

    }

    public function show($id)
    {
     try{   $user= user::findOrFail($id);}

     catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e){
            return response()->json("Bad Request", 404);
        }
        return response()->json($user, 200);
    }


    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|between:2,100',
            'email' => 'required|string|email|max:100|unique:users',
            'password' => 'required|string|min:6',
            'date' => ['required', 'date', new ValidDateFormat],
            'phone_number' => 'required|string|max:20',
            'address' => 'required|string|max:255',
            'cv'=>'nullable|string|max:255',
            'education'=>'nullable|string|max:255',
           'experiences'=>'nullable|string|max:255',
           'skills'=>'nullable|string|max:255',
            'previous_courses'=>'nullable|string|max:255',
            'role'=>'nullable|string|max:20',
            'gender' => 'nullable|string|between:2,100',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 404);
        }
        try{
        $user = User::findOrFail($id);
       }catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e){
            return response()->json("Bad Request", 404);
        }

        $user->update($request->all());
        if($user){
            return response()->json(['message' => 'Updated Successfully',
            'user' => $user
             ], 200);
        }
    }

    public function destroy($id)
    {
    try {
        $user = User::findOrFail($id);
    } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
        return response("Bad Request", 404);
    }

    $user->delete($id);
    return response('Deleted Successfully', 200);
}

}
