<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\review;
use App\Rules\ValidDateFormat;
use Illuminate\Support\Facades\Http;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;


class ReviewController extends Controller
{
    public function analyzeReviews(Request $request)
    {
        $validator=Validator::make($request->all(),
            ['text'=>'required|string',]);

            if ($validator->fails()) {
                return response()->json($validator->errors()->toJson(), 404);
            }


        $validatedData = $request->input('text');

        try {
                $client = new Client();
                $url = 'http://192.168.1.85:8080/analyze-sentiment/';  // Python API endpoint

                $response = $client->post($url, [
                    'json' => ['text' => $validatedData],
                    'timeout'=>30,
                ]);


                    $data = json_decode($response->getBody()->getContents(), true);

                    return response()->json($data, $response->getStatusCode());
                } catch (\GuzzleHttp\Exception\RequestException $e) {
                    Log::error('Sentiment analysis failed: ' . $e->getMessage());
                    return response()->json(['error' => 'Failed to analyze sentiment'], 500);
                }catch (\Exception $e) {
                    Log::error('general Exception: ' . $e->getMessage());
                    return response()->json(['error' => 'Failed to analyze sentiment'], 500);
            }
        }
    }
