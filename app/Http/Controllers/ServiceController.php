<?php

namespace App\Http\Controllers;
use App\Models\design;
use App\Models\developer;
use App\Models\other;
use App\Models\sales_and_marketing;
use App\Models\service;
use App\Models\company;
use App\Models\teach_and_learn;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Storage;
use App\Models\contract;
class ServiceController extends Controller
{

    public function index()
    {
         // استخراج المستخدم من التوكن
        $user = JWTAuth::parseToken()->authenticate();

        // جلب الخدمات الخاصة بهذا المستخدم فقط
        $services = Service::where('user_id', $user->id)->get();
     //  $services=service::get();
       return response()->json(['message' => 'OK',
       'services' => $services
        ], 200);

    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'description'=>'required|string|max:255',
            'name' => 'nullable|string|between:2,100',
            'profile_picture'=>'required|file|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'email'=>'nullable|string|email|max:100',
            'phone'=>'required||string|max:9999999999',
            'cv' => 'nullable|file||max:2550|mimes:pdf',
            'type' => 'required|string|max:10',
            'category_type'=> 'required|string|max:30',
            'supplier_id'=>'nullable|integer|exists:suppliers,id',
            'instructor' => 'nullable|string|max:255',
            'level' => 'nullable|string|max:255',
            'subject' => 'nullable|string|max:255',
            'address'=>'nullable|string|max:255',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 404);
        }
        $profile_picture_path = $request->file('profile_picture')->store('public/profile_pictures');
        $profile_picture_url = asset(Storage::url($profile_picture_path)); // Generate full URL

        // Store the CV if it exists
        $cv_path = $request->file('cv') ? $request->file('cv')->store('public/cvs') : null;
        $cv_url = $cv_path ? asset(Storage::url($cv_path)) : null;

        // استخراج المستخدم من التوكن
        $user = JWTAuth::parseToken()->authenticate();

        // إنشاء الخدمة وربطها بالمستخدم
        $service = Service::create(array_merge(
            $request->all(),
            [
                'user_id' => $user->id,
                'profile_picture' => $profile_picture_url,
                'cv' => $cv_url,]
        ));
     //   $service = Service::create($request->all());

        if ($service) {
            $categoryData = [
                'type' => $service->type,
                'name' => $service->name,
                'rate' =>$service->rate,
                'profile_picture' => $profile_picture_url,
               'service_id' => $service->id ?? null,
               'supplier_id' => $service->supplier_id ?? null,
            ];

            switch ($service->category_type) {
                case 'تطوير':
                    developer::create($categoryData);
                    break;
                case 'تصميم':
                    design::create($categoryData);
                    break;
                case 'تسويق و مبيعات':
                    sales_and_marketing::create($categoryData);
                    break;
                case 'تعليم و تدريب':
                    $categoryData['instructor'] = $request->input('instructor');
                    $categoryData['level'] = $request->input('level');
                    $categoryData['subject'] = $request->input('subject');
                    teach_and_learn::create($categoryData);
                    break;
                case 'غير ذلك':
                   other::create($categoryData);
                    break;
                    case 'تنظيف':
                        case 'نقل':
                        case 'تامين':
                        case 'تامين صحي':
                            $companyData = [
                                'name' => $service->name,
                                'type' => $service->type,
                                'address' => $service->address,
                                'email' => $service->email,
                                'phone' => $service->phone,
                                'profile_picture' => $service->profile_picture,
                                'description' => $service->description,
                                'client_id' => $service->client_id ?? null,
                                'supplier_id' => $service->supplier_id ?? null,
                            ];
                            company::create($companyData);
                            break;
                default:
                    return response()->json("Invalid category_type", 404);
            }

       /*   $contracts = $service->contracts()->get();

        return response()->json([
            'service' => $service,
            'contracts' => $contracts,
        ], 200);*/
           return response()->json($service, 200);
        }

        return response()->json("Bad Request", 404);
    }

     /*   $service = service::create($request->all());
        if($service){
            return response()->json($service, 200);
        }
        return response()->json("Bad Request", 404);
    }*/

    public function show()
    {
      //  $services=service::get();

    $services = Service::with('contracts')->get();
        return response()->json(['message' => 'OK',
        'services' => $services
         ], 200);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'description'=>'required|string|max:255',
            'name' => 'nullable|string|between:2,100',
            'profile_picture'=>'required|file|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'email'=>'nullable|string|email|max:100',
            'phone'=>'required||string|max:9999999999',
            'cv' => 'nullable|string|max:255',
            'type' => 'required|string|max:10',
            'category_type'=> 'required|string|max:10',
            'supplier_id'=>'nullable|integer|exists:suppliers,id',

        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 404);
        }
        $profile_picture_path = $request->file('profile_picture')->store('public/profile_pictures');
        $profile_picture_url = asset(Storage::url($profile_picture_path)); // Generate full URL

        $cv_path = $request->file('cv') ? $request->file('cv')->store('public/cvs') : null;
        $cv_url = $cv_path ? asset(Storage::url($cv_path)) : null;

        try{
        $service = service::findOrFail($id);
       }catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e){
            return response()->json("Bad Request", 404);
        }

        $service->update($request->all(),[
            'profile_picture' => $profile_picture_url,
            'cv' => $cv_url,
        ]);
        if($service){
            return response()->json(['message' => 'Updated Successfully',
            'service' => $service
             ], 200);
        }
    }


    public function destroy($id)
    {
    try {
        $service = service::findOrFail($id);
    } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
        return response("Bad Request", 404);
    }

    $service->delete($id);
    return response('Deleted Successfully', 200);
}
}
