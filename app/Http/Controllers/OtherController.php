<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use App\Models\other;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class OtherController extends Controller
{
    public function index()
    {
       $other=other::get();
       return response()->json(['message' => 'OK',
       'other' => $other
        ], 200);

    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'type' => 'required|string|max:10',
            'name'=>'required|string|max:255',
            'profile_picture'=>'required|file|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'service_id'=>'nullable|integer|exists:services,id',

        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 404);
        }
        $profile_picture_path = $request->file('profile_picture')->store('public/profile_pictures');
        $profile_picture_url = asset(Storage::url($profile_picture_path)); // Generate full URL

        $other = other::create($request->all(),[
            'profile_picture' => $profile_picture_url,
        ]);
        if($other){
            return response()->json($other, 200);
        }
        //unable to create other
        return response()->json("Bad Request", 404);
    }

    public function show($id)
    {
        try{  $other= other::findOrFail($id);}
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e){
            return response()->json("Bad Request", 404);
        }
        return response()->json($other, 200);
    }


    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'type' => 'required|string|max:10',
            'name'=>'required|string|max:255',
            'profile_picture'=>'required|file|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'service_id'=>'nullable|integer|exists:services,id',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 404);
        }
        $profile_picture_path = $request->file('profile_picture')->store('public/profile_pictures');
        $profile_picture_url = asset(Storage::url($profile_picture_path)); // Generate full URL

        try{
        $other = other::findOrFail($id);}
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e){
            return response()->json("Bad Request", 404);
        }

        $other->update($request->all(),[
            'profile_picture' => $profile_picture_url,
        ]);
        if($other){
            return response()->json(['message' => 'Updated Successfully',
            'other' => $other
             ], 200);
        }
    }

    public function destroy($id)
    {
        try {
        $other= other::findOrFail($id);}
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response("Bad Request", 404);
        }
       $other->delete($id);
       if($other){
        return response('Deleted Successfully', 200);
    }
    }
}
