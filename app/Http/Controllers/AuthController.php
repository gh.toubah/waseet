<?php

namespace App\Http\Controllers;
use App\Rules\ValidDateFormat;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Storage;

class AuthController extends Controller
{
    public function __construct() {
        $this->middleware('auth:api', ['except' => ['login', 'register','resetPassword']]);
    }
    public function login(Request $request){
    	$validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|string|min:6',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 404);
        }
        if (! $token = auth()->attempt($validator->validated())) {
            return response()->json(['error' => 'Unauthorized'], 404);
        }
        return $this->createNewToken($token);
    }
    public function register(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|between:2,100',
            'email' => 'required|string|email|max:100|unique:users',
            'profile_picture'=>'nullable|file|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'password' => 'required|string|min:6',
            'date' => ['required', 'date', new ValidDateFormat],
            'phone_number' => 'required|string|max:20',
            'address' => 'required|string|max:255',
            'cv'=>'nullable|string|max:255',
            'education'=>'nullable|string|max:255',
           'experiences'=>'nullable|string|max:255',
           'skills'=>'nullable|string|max:255',
            'previous_courses'=>'nullable|string|max:255',

        ]);
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 404);
        }
        $profile_picture_path = $request->file('profile_picture')->store('public/profile_pictures');
        $profile_picture_url = asset(Storage::url($profile_picture_path)); // Generate full URL
        $user = User::create(array_merge(
                    $validator->validated(),
                    ['password' => bcrypt($request->password),
                    'profile_picture' => $profile_picture_url
                    ]
                ));
        return response()->json([
            'message' => 'User successfully registered',
            'user' => $user
        ], 200);
    }
    public function resetPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|string|min:6',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 404);
        }

        $user = User::where('email', $request->email)->first();

        if (!$user) {
            return response()->json(['error' => 'User not found'], 404);
        }

        $user->password = Hash::make($request->password);
        $user->save();

        return response()->json(['message' => 'Password reset successfully'], 200);
    }

    public function logout() {
        auth()->logout();
        return response()->json(['message' => 'User successfully signed out']);
    }
    public function refresh() {
        return $this->createNewToken(auth()->refresh());
    }
    public function userProfile() {
        return response()->json(auth()->user());
    }
    protected function createNewToken($token){
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user' => auth()->user()
        ]);
    }
}
