<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use App\Models\employee;
use App\Models\User;
use App\Models\company;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    public function index()
    {
       $employees=employee::get();
       return response()->json(['message' => 'OK',
       'employees' => $employees
        ], 200);

    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'status' => 'required|string|max:10',
            'user_id'=>'nullable|integer|exists:users,id',
            'company_id'=>'nullable|integer|exists:companies,id',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 404);
        }
        $employee = employee::create($request->all());
        if($employee){
            return response()->json($employee, 200);
        }
        return response()->json("Bad Request", 404);
    }


    public function show($id)
    {
    try{    $employee= employee::findOrFail($id);}
    catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e){
        return response()->json("Bad Request", 404);
    }
    return response()->json($employee, 200);
    }


    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'status' => 'required|string|max:10',
            'user_id'=>'nullable|integer|exists:users,id',
            'company_id'=>'nullable|integer|exists:companies,id',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }
        try{
        $employee = employee::findOrFail($id);}
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e){
            return response()->json("Bad Request", 404);
        }

        $employee->update($request->all());
        if($employee){
            return response()->json(['message' => 'Updated Successfully',
            'employee' => $employee
             ], 200);
       // return response()->json($validator->errors(), 422);
    }}

    public function destroy($id)
    {
        try {
            $employee = employee::findOrFail($id);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response("Bad Request", 404);
        }

        $employee->delete($id);
        return response('Deleted Successfully', 200);
    }
}
