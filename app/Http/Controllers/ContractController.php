<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use App\Rules\ValidDateFormat;
use App\Models\contract;
use App\Models\offer;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class ContractController extends Controller
{
    public function index()
    {
        $user = JWTAuth::parseToken()->authenticate();

        $contracts = contract::where('user_id', $user->id)->get();
     //  $contracts=contract::get();
       return response()->json(['message' => 'OK',
       'contracts' => $contracts
        ], 200);


    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|between:2,100',
            'status' => 'required|string|max:10',
            'price'=>'required|integer|between:100,15000000',
            'description'=>'required|string|max:255',
            'notes'=>'required|string|max:255',
            'rate'=>'nullable|numeric|between:1,5',
            'expected_duration'=> ['required', 'date', new ValidDateFormat],
            'offer_id'=>'nullable|integer|exists:offers,id',
            'service_id'=>'nullable|integer|exists:services,id',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 404);
        }
        $user = JWTAuth::parseToken()->authenticate();


        $contract = contract::create(array_merge(
            $request->all(),
            ['user_id' => $user->id]
        ));
      //  $contract = contract::create($request->all());
        if($contract){
            return response()->json($contract, 200);
        }
        return response()->json("Bad Request", 404);
    }


    public function show($id)
    {
        try{    $contract= contract::findOrFail($id);}
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e){
            return response()->json("Bad Request", 404);
        }
        return response()->json($contract, 200);
    }


    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|between:2,100',
            'status' => 'required|string|max:10',
            'price'=>'required|integer|between:100,15000000',
            'description'=>'required|string|max:255',
            'notes'=>'required|string|max:255',
            'rate'=>'nullable|numeric|between:1,5',
            'expected_duration'=>['required', 'date', new ValidDateFormat],
            'offer_id'=>'nullable|integer|exists:offers,id',
            'service_id'=>'nullable|integer|exists:services,id',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 404);
        }
        try{
            $contract = contract::findOrFail($id);}
            catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e){
                return response()->json("Bad Request", 404);
            }

            $contract->update($request->all());
            if($contract){
                return response()->json(['message' => 'Updated Successfully',
                'contract' => $contract
                 ], 200);
        }
       // return response()->json($validator->errors(), 422);
    }

    public function destroy($id)
    {
        try {
            $contract = contract::findOrFail($id);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response("Bad Request", 404);
        }

        $contract->delete($id);
        return response('Deleted Successfully', 200);
    }}
