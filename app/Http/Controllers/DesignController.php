<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use App\Models\design;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class DesignController extends Controller
{
    public function index()
    {
       $design=design::get();
       return response()->json(['message' => 'OK',
       'design' => $design
        ], 200);

    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'type' => 'required|string|max:10',
            'name'=>'required|string|max:255',
            'profile_picture'=>'required|file|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'service_id'=>'nullable|integer|exists:services,id',

        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 404);
        }
        $profile_picture_path = $request->file('profile_picture')->store('public/profile_pictures');
        $profile_picture_url = asset(Storage::url($profile_picture_path)); // Generate full URL

        $design = design::create($request->all(),[
            'profile_picture' => $profile_picture_url,
        ]);
        if($design){
            return response()->json($design, 200);
        }
        //unable to create design
        return response()->json("Bad Request", 404);
    }

    public function show($id)
    {
        try{  $design= design::findOrFail($id);}
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e){
            return response()->json("Bad Request", 404);
        }
        return response()->json($design, 200);
    }


    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'type' => 'required|string|max:10',
            'name'=>'required|string|max:255',
            'profile_picture'=>'required|file|mimes:jpeg,png,jpg,gif,svg|max:2048',
           'service_id'=>'nullable|integer|exists:services,id',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 404);
        }
        $profile_picture_path = $request->file('profile_picture')->store('public/profile_pictures');
        $profile_picture_url = asset(Storage::url($profile_picture_path)); // Generate full URL
        try{
        $design = design::findOrFail($id);}
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e){
            return response()->json("Bad Request", 404);
        }

        $design->update($request->all(),[
            'profile_picture' => $profile_picture_url,
        ]);
        if($design){
            return response()->json(['message' => 'Updated Successfully',
            'design' => $design
             ], 200);
        }
    }

    public function destroy($id)
    {
        try {
        $design= design::findOrFail($id);}
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response("Bad Request", 404);
        }
       $design->delete($id);
       if($design){
        return response('Deleted Successfully', 200);
    }
    }
}
