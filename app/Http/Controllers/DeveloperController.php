<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use App\Models\developer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class DeveloperController extends Controller
{
    public function index()
    {
       $developer=developer::get();
       return response()->json(['message' => 'OK',
       'developer' => $developer
        ], 200);

    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'type' => 'required|string|max:10',
            'name'=>'required|string|max:255',
            'profile_picture'=>'required|file|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'service_id'=>'nullable|integer|exists:services,id',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 404);
        }
        $profile_picture_path = $request->file('profile_picture')->store('public/profile_pictures');
        $profile_picture_url = asset(Storage::url($profile_picture_path)); // Generate full URL

        $developer = developer::create($request->all(),[
            'profile_picture' => $profile_picture_url,
        ]);
        if($developer){
            return response()->json($developer, 200);
        }
        //unable to create developer
        return response()->json("Bad Request", 404);
    }

    public function show($id)
    {
        try{  $developer= developer::findOrFail($id);}
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e){
            return response()->json("Bad Request", 404);
        }
        return response()->json($developer, 200);
    }


    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'type' => 'required|string|max:10',
            'name'=>'required|string|max:255',
            'profile_picture'=>'required|file|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'service_id'=>'nullable|integer|exists:services,id',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 404);
        }
        $profile_picture_path = $request->file('profile_picture')->store('public/profile_pictures');
        $profile_picture_url = asset(Storage::url($profile_picture_path)); // Generate full URL
        try{
        $developer = developer::findOrFail($id);}
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e){
            return response()->json("Bad Request", 404);
        }

        $developer->update($request->all(),[
            'profile_picture' => $profile_picture_url,
        ]);
        if($developer){
            return response()->json(['message' => 'Updated Successfully',
            'developer' => $developer
             ], 200);
        }
    }

    public function destroy($id)
    {
        try {
        $developer= developer::findOrFail($id);}
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response("Bad Request", 404);
        }
       $developer->delete($id);
       if($developer){
        return response('Deleted Successfully', 200);
    }
    }
}
