<?php

namespace App\Http\Controllers;
//use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\supplier;
use App\Models\User;
use Illuminate\Http\Request;

class SupplierController extends Controller
{
    public function index()
    {
       // return supplier::all();
       $suppliers=supplier::get();
       return response()->json(['message' => 'OK',
       'suppliers' => $suppliers
        ], 200);

    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'type' => 'required|string|max:10',
            'about'=>'required|string|max:255',
            'project_history'=>'nullable|string|max:255',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 404);
        }
        $supplier = Supplier::create($request->all());
        if($supplier){
            return response()->json($supplier, 200);
        }
        return response()->json("Bad Request", 404);
    }


    public function show($id)
    {
     try{   $Supplier= Supplier::findOrFail($id);}

     catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e){
         // if($Supplier){
            return response()->json("Bad Request", 404);
        }
        return response()->json($Supplier, 200);
    }


    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'type' => 'required|string|max:10',
            'about'=>'required|string|max:255',
            'project_history'=>'required|string|max:255',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 404);
        }
        try{
        $supplier = Supplier::findOrFail($id);
       // if(!$supplier){
       }catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e){
            return response()->json("Bad Request", 404);
        }

        $supplier->update($request->all());
        if($supplier){
            return response()->json(['message' => 'Updated Successfully',
            'supplier' => $supplier
             ], 200);
        }
       // return response()->json($validator->errors(), 422);
    }

    public function destroy($id)
    {
    try {
        $supplier = supplier::findOrFail($id);
    } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
        return response("Bad Request", 404);
    }

    $supplier->delete($id);
    return response('Deleted Successfully', 200);
}
   /*  $supplier= supplier::findOrFail($id);
        if(!$supplier){
            return response("supplier not found", 404);
        }
       $supplier->delete($id);
       if($supplier){
        return response('Deleted Successfully', 200);
    }
    }*/
   /* public function deleteall(){
        DB::table('suppliers')->truncate();
    return response()->json(['message' => 'All records deleted and auto-increment reset'], 200);
    }*/
}
