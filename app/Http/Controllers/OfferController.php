<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use App\Models\offer;
use App\Models\service;
use App\Models\client;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class OfferController extends Controller
{
    public function index()
    {
            $user = JWTAuth::parseToken()->authenticate();

            $offers = offer::where('user_id', $user->id)->get();
    //   $offers=offer::get();
       return response()->json(['message' => 'OK',
       'offers' => $offers
        ], 200);

    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'description'=>'required|string|max:255',
            'notes'=>'required|string|max:255',
            'service_id'=>'nullable|integer|exists:services,id',
           'client_id'=>'nullable|integer|exists:clients,id',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 404);
        }

          $user = JWTAuth::parseToken()->authenticate();


          $offer = offer::create(array_merge(
              $request->all(),
              ['user_id' => $user->id]
          ));
       // $offer = offer::create($request->all());
        if($offer){
            return response()->json($offer, 200);
        }
        return response()->json("Bad Request", 404);
    }


    public function show($id)
    {
        try{  $offer= offer::findOrFail($id);}
       // if($offer){
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e){
            return response()->json("Bad Request", 404);
        }
        return response()->json($offer, 200);
      //  return response()->json($offer->errors(), 422);
    }


    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'description'=>'required|string|max:255',
            'notes'=>'required|string|max:255',
            'service_id'=>'nullable|integer|exists:services,id',
            'client_id'=>'nullable|integer|exists:clients,id'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 404);
        }
        try{
        $offer = offer::findOrFail($id);}
      //  if(!$offer){
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e){
            return response()->json("Bad Request", 404);
        }

        $offer->update($request->all());
        if($offer){
            return response()->json(['message' => 'Updated Successfully',
            'offer' => $offer
             ], 200);
        }
       // return response()->json($validator->errors(), 422);
    }

    public function destroy($id)
    {
        try {
        $offer= offer::findOrFail($id);}
      //  if(!$offer){
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response("Bad Request", 404);
        }
       $offer->delete($id);
       if($offer){
        return response('Deleted Successfully', 200);
    }
    }
}
