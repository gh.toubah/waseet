<?php

namespace App\Http\Controllers;

use App\Models\client;
use App\Http\contracts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
class ClientController extends Controller
{
    public function index()
    {
       // return client::all();
       $clients=client::get();
       return response()->json(['message' => 'OK',
       'clients' => $clients
        ], 200);

    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'about'=>'required|string|max:255',
            'profile_picture'=>'required|file|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 404);
        }
        $profile_picture_path = $request->file('profile_picture')->store('public/profile_pictures');
        $profile_picture_url = asset(Storage::url($profile_picture_path)); // Generate full URL

        $client = client::create($request->all(),[
            'profile_picture' => $profile_picture_url,
        ]);
        if($client){
            return response()->json($client, 200);
        }
        //unable to create client
        return response()->json("Bad Request", 404);
    }


    public function show($id)
    {
        try{    $client= client::findOrFail($id);}
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e){
            return response()->json("Bad Request", 404);
        }
        return response()->json($client, 200);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'about'=>'required|string|max:255',
            'profile_picture'=>'required|file|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 404);
        }
        $profile_picture_path = $request->file('profile_picture')->store('public/profile_pictures');
        $profile_picture_url = asset(Storage::url($profile_picture_path)); // Generate full URL

        try{
            $client = client::findOrFail($id);}
            catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e){
                return response()->json("Bad Request", 404);
            }

            $client->update($request->all(),[
                'profile_picture' => $profile_picture_url,
            ]);
            if($client){
                return response()->json(['message' => 'Updated Successfully',
                'client' => $client
                 ], 200);
        }
    }

    public function destroy($id)
    {
        try {
            $client = client::findOrFail($id);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response("Bad Request", 404);
        }

        $client->delete($id);
        return response('Deleted Successfully', 200);
   // return response()->json($validator->errors(), 422);
}
        //return response()->json($client->errors(), 422);
}


