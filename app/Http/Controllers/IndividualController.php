<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use App\Models\individual;
use App\Models\supplier;
use App\Models\client;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class IndividualController extends Controller
{
    public function index()
    {
       $individuals=individual::get();
       return response()->json(['message' => 'OK',
       'individuals' => $individuals
        ], 200);

    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'=>'required|string|max:255',
            'profile_picture'=>'required|file|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'status' => 'required|string|max:10',
           'user_id'=>'nullable|integer|exists:users,id',
            'supplier_id'=>'nullable|integer|exists:suppliers,id',
            'client_id'=>'nullable|integer|exists:clients,id',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 404);
        }
        $profile_picture_path = $request->file('profile_picture')->store('public/profile_pictures');
        $profile_picture_url = asset(Storage::url($profile_picture_path)); // Generate full URL

        $individual = individual::create($request->all(),[
            'profile_picture' => $profile_picture_url,
        ]);
        if($individual){
            return response()->json($individual, 200);
        }
        return response()->json("Bad Request", 404);
    }


    public function show($id)
    {
     try{   $individual= individual::findOrFail($id);}

     catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e){
            return response()->json("Bad Request", 404);
        }
        return response()->json($individual, 200);
    }


    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name'=>'required|string|max:255',
            'profile_picture'=>'required|file|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'status' => 'required|string|max:10',
            'user_id'=>'nullable|integer|exists:users,id',
            'supplier_id'=>'nullable|integer|exists:suppliers,id',
            'client_id'=>'nullable|integer|exists:clients,id',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 404);
        }
        $profile_picture_path = $request->file('profile_picture')->store('public/profile_pictures');
        $profile_picture_url = asset(Storage::url($profile_picture_path)); // Generate full URL
        try{
        $individual = individual::findOrFail($id);
       }catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e){
            return response()->json("Bad Request", 404);
        }

        $individual->update($request->all(),[
            'profile_picture' => $profile_picture_url,
        ]);
        if($individual){
            return response()->json(['message' => 'Updated Successfully',
            'individual' => $individual
             ], 200);
        }
       // return response()->json($validator->errors(), 422);
    }

    public function destroy($id)
    {
    try {
        $individual = individual::findOrFail($id);
    } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
        return response("Bad Request", 404);
    }

    $individual->delete($id);
    return response('Deleted Successfully', 200);
}
}
