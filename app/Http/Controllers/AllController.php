<?php

namespace App\Http\Controllers;
use App\Models\all;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class AllController extends Controller
{
    public function index()
    {
       $all=all::get();
       return response()->json(['message' => 'OK',
       'all' => $all
        ], 200);

    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'profile_picture'=>'required|string|max:255',

        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 404);
        }
        $all = all::create($request->all());
        if($all){
            return response()->json($all, 200);
        }
        return response()->json("Bad Request", 404);
    }
}
