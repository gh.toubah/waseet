<?php

namespace App\Http\Controllers;
use App\Rules\ValidDateFormat;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;

class authwController extends Controller
{
    public function __construct() {
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
    }
    public function login(Request $request)
    {
            $validator = Validator::make($request->all(), [
                'email' => 'required|email',
                'password' => 'required|string|min:6',
            ]);
            if ($validator->fails()) {
                return response()->json($validator->errors(), 404);
            }
            if (! $token = auth()->attempt($validator->validated())) {
                return response()->json(['error' => 'Unauthorized'], 404);
            }
            return $this->createNewToken($token);
        }

    public function register(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|between:2,100',
            'email' => 'required|string|email|max:100|unique:users',
            'password' => 'required|string|min:6',
            'date' => ['required', 'date', new ValidDateFormat],
            'phone_number' => 'required|string|max:20',
            'address' => 'required|string|max:255',
            'cv'=>'nullable|string|max:255',
            'education'=>'required|string|max:255',
           'experiences'=>'nullable|string|max:255',
            'previous_courses'=>'nullable|string|max:255',
            'rate'=>'nullable|numeric|between:1,5',
            'role'=>'nullable|string|max:20',
            'gender' => 'nullable|string|between:2,100',


        ]);
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 404);
        }

        $user = User::create(array_merge(
                    $validator->validated(),
                    ['password' => bcrypt($request->password)],
                    ['role' => 'user']
                ));
        return response()->json([
            'message' => 'User successfully registered',
            'user' => $user
        ], 200);
    }
    protected function createNewToken($token){
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user' => auth()->user()
        ]);
    }
}
