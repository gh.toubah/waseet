<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ValidDateFormat implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        // التحقق من أن التنسيق هو YYYY-MM-DD
        return preg_match('/^\d{4}-\d{2}-\d{2}$/', $value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute must be a valid date format (YYYY-MM-DD).';
    }
}
