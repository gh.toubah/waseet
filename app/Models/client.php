<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class client extends Model
{
    use HasFactory;
    protected $fillable = [
        'about',
        'profile_picture',
    ];

    public function offers(){
        return $this->hasMany(offers::class);
    }

    public function feedback(){
        return $this->hasMany(feedback::class);
    }

    public function notifications(){
        return $this->hasMany(notifications::class);
    }
}


