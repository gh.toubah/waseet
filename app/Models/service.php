<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class service extends Model
{
    use HasFactory;
    protected $fillable = [
        'description',
        'name',
        'cv',
        'phone',
        'category_type',
        'type',
        'supplier_id',
        'profile_picture',
        'email',
        'level',
        'instructor',
        'subject',
        'address',
        'user_id'
    ];
   public function offers(){
        return $this->hasMany(offers::class);
    }

    public function suppliers()
    {
        return $this->hasMany(suppliers::class);
    }
    public function contracts()
    {
        return $this->hasMany(Contract::class);
    }
}
