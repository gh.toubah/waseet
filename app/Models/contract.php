<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class contract extends Model
{
    use HasFactory;
    protected $fillable = [
        'status',
        'price',
        'description',
        'notes',
        'expected_duration',
        'offer_id',
        'name',
        'user_id',
        'rate',
        'service_id'
    ];
    public function service()
    {
        return $this->belongsTo(Service::class);
    }
}
