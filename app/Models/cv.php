<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class cv extends Model
{
    use HasFactory;
    protected $fillable = [
        'email',
        'name',
        'profile_picture',
       'phone_number',
       'address',
       'objective',
       'education',
       'previous_courses',
       'experince',
       'skills'
    ];
}
