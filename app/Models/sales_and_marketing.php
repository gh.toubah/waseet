<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class sales_and_marketing extends Model
{
    use HasFactory;
    protected $fillable = [
        'type',
        'name',
        'profile_picture',
        'service_id',
    ];
}
