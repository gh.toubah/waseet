<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class company extends Model
{
    use HasFactory;
    protected $fillable = [
        'address',
        'name',
        'email',
        'phone',
        'description',
        'client_id',
       'supplier_id',
        'profile_picture',
        'type'
    ];
    public function employees()
    {
        return $this->hasMany(employees::class);
    }
}
