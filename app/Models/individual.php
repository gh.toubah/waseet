<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class individual extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'profile_picture',
        'status',
        'user_id',
        'supplier_id',
        'client_id'
    ];
}
