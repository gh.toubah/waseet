<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class employee extends Model
{
    use HasFactory;
    protected $fillable = [
        'status',
        'user_id',
        'company_id'
    ];

    public function company()
    {
        return $this->belongsTo(company::class);
    }
}
