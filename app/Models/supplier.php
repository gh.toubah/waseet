<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class supplier extends Model
{
    use HasFactory;
    protected $fillable = [
        'type',
        'project_history',
        'about',
    ];

    public function feedback(){
        return $this->hasMany(feedback::class);
    }

    public function notifications(){
        return $this->hasMany(notifications::class);
    }

    public function service()
    {
        return $this->belongsTo(services::class);
    }
}
