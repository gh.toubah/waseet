<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class offer extends Model
{
    use HasFactory;
    protected $fillable = [
        'description',
        'notes',
        'service_id',
        'client_id',
        'user_id'
    ];

   public function services(){
        return $this->belongsTo(services::class);
    }

    public function clients(){
        return $this->belongsTo(clients::class);
    }
}
