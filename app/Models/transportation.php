<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class transportation extends Model
{
    use HasFactory;
    protected $fillable = [
        'rented_to',
        'hours_rented',
        'status_driver'
    ];
}
