<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class teach_and_learn extends Model
{
    use HasFactory;
    protected $fillable = [
        'instructor',
        'subject',
        'level',
        'type',
        'name',
        'profile_picture',
       'service_id',
    ];
}
